module gitlab.com/juan794/gopacket-ble-mesh

go 1.16

require (
	github.com/go-ble/ble v0.0.0-20200407180624-067514cd6e24
	github.com/google/gopacket v1.1.19
)
