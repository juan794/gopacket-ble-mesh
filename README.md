# Gopacket BLE Mesh

Gopacket definitions for BLE Mesh

# Structs

- Advertising 
    - Length uint8
    - Type   uint8   
    - Beacon (Type == 0x2b)
        - Type uint8
        - Unprovisioned Device (Type == 0x00)
            - UUID [16]byte
            - OOB [2]byte
                - Other bool
                - URI bool
                - Code2D bool
                - NFC bool
                - Number bool
                - String bool
                - Reserved [4]bool
                - OnBox bool
                - InsideBox bool
                - Paper bool
                - Manual bool
                - OnDevice bool
            - URI Hash [4]byte (!optional)
        - Secure Network (Type == 0x01)
            - Type uint8
            - Flags uint8
                - KeyRefresh bool
                - IVUpdate bool
                - Reserved [6]bool
            - NetworkID [8]byte
            - IVIndex [4]byte
            - Authentication [8]byte
    - PB-ADV (Type == 0x29)
        - LinkID uint32
        - TxNumber uint8
        - Generic Provisioning PDU
            - GenericProvisioningControlFormat uint8 (!2 bits only)
            - Transaction Start (GenericProvisioningControlFormat == 0x00)
                - SegNumber uint8 (!6 bits only)
                - Length uint16
                - FCS uint8
                - PayloadFragment []byte (!option SegNumber > 0)
                - Mesh Provisioning (!option SegNumber == 0)
                    - Type uint8
                    - Parameters []byte #EXPAND!!!
            - Transaction Acknowledgement (GenericProvisioningControlFormat == 0x01)
                - Padding uint8 (!6 bits only)
            - Transaction Continuation (GenericProvisioningControlFormat == 0x02)
                - SegmentIndex uint8 (!6 bits only)
                - PayloadFragment []byte
            - Bearer Control (GenericProvisioningControlFormat == 0x03)
                - BearerOpcode uint8 (!6 bits only)
                - UUID [16]byte (!optional BearerOpcode == 0x00)
                - Reason uint8 (!optional BearerOpcode == 0x02)
    - Mesh (Type == 0x2a)
        - Message []byte