package layers

import (
	"encoding/binary"
	"fmt"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type GenericProvisioningType uint8

const (
	GenericProvTypeTxStart           = 0
	GenericProvTypeTxAcknowledgement = 1
	GenericProvTypeTxContinuation    = 2
	GenericProvTypeBearerControl     = 3
)

func (g GenericProvisioningType) LayerType() gopacket.LayerType {
	switch g {
	case GenericProvTypeTxAcknowledgement:
		return gopacket.LayerTypeZero
	default:
		return gopacket.LayerTypeDecodeFailure
	}
}

type PBADV struct {
	layers.BaseLayer

	LinkID   uint32
	TxNumber uint8

	gpcf GenericProvisioningType // ignore fuzzer
}

func decodePBADV(data []byte, p gopacket.PacketBuilder) error {
	pbadv := &PBADV{}
	if err := pbadv.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(pbadv)
	return p.NextDecoder(pbadv.NextLayerType())
}

func (p *PBADV) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 5 {
		df.SetTruncated()
		return fmt.Errorf("invalid PB-ADV packet. Length %v less than %v", len(data), 5)
	}

	p.LinkID = binary.BigEndian.Uint32(data[0:4])
	p.TxNumber = uint8(data[4])

	p.gpcf = GenericProvisioningType(data[5] & 0x03)

	p.Contents = data[:5]
	p.Payload = data[5:]
	return nil
}

func (p *PBADV) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	return nil
}

// NextLayerType function
func (p *PBADV) NextLayerType() gopacket.LayerType {
	return p.gpcf.LayerType()
}

// LayerType function
func (p *PBADV) LayerType() gopacket.LayerType {
	return LayerPBADVType
}
