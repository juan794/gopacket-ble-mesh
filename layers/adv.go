package layers

import (
	"fmt"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type AdvertisingType uint8

const (
	AdvertisingTypeBeacon = 0x2b
	AdvertisingTypePBADV  = 0x29
	AdvertisingTypeMesh   = 0x2a
)

func (a AdvertisingType) LayerType() gopacket.LayerType {
	switch a {
	case AdvertisingTypeBeacon:
		return LayerBeaconType
	case AdvertisingTypePBADV:
		return LayerPBADVType
	default:
		return gopacket.LayerTypePayload
	}
}

type Advertising struct {
	layers.BaseLayer

	Length uint8
	Type   AdvertisingType
}

func decodeAdvertising(data []byte, p gopacket.PacketBuilder) error {
	advertising := &Advertising{}
	if err := advertising.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(advertising)
	return p.NextDecoder(advertising.NextLayerType())
}

func (a *Advertising) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 3 {
		df.SetTruncated()
		return fmt.Errorf("invalid Advertising packet. Length %v less than %v", len(data), 3)
	}

	a.Length = uint8(data[0])
	a.Type = AdvertisingType(data[1])

	if len(data) < int(a.Length+1) {
		df.SetTruncated()
		return fmt.Errorf("invalid Advertising length. Length %v less than %v", len(data), a.Length)
	}

	a.Contents = data[:2]
	a.Payload = data[2:]

	return nil
}

func (a *Advertising) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(2)
	if err != nil {
		return err
	}

	if opts.FixLengths {
		a.Length = uint8(len(b.Bytes()))
	}

	bytes[0] = a.Length
	bytes[1] = uint8(a.Type)

	return nil
}

// NextLayerType function
func (a *Advertising) NextLayerType() gopacket.LayerType {
	return a.Type.LayerType()
}

// LayerType function
func (a *Advertising) LayerType() gopacket.LayerType {
	return LayerAdvertisingType
}
