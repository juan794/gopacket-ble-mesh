package layers

import (
	"fmt"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type MeshProvisioningType uint8

const (
	MeshProvisioningTypeInvite       MeshProvisioningType = 0
	MeshProvisioningTypeCapabilities MeshProvisioningType = 1
	MeshProvisioningTypeStart        MeshProvisioningType = 2
	MeshProvisioningTypePublicKey    MeshProvisioningType = 3
	MeshProvisioningTypeInput        MeshProvisioningType = 4
	MeshProvisioningTypeConfirmation MeshProvisioningType = 5
	MeshProvisioningTypeRandom       MeshProvisioningType = 6
	MeshProvisioningTypeData         MeshProvisioningType = 7
	MeshProvisioningTypeComplete     MeshProvisioningType = 8
	MeshProvisioningTypeFail         MeshProvisioningType = 9
)

func (m MeshProvisioningType) LayerType() gopacket.LayerType {
	switch m {
	case MeshProvisioningTypeInvite:
		return LayerMeshProvInviteType
	default:
		return gopacket.LayerTypeDecodeFailure
	}
}

type MeshProvisioning struct {
	layers.BaseLayer

	Type MeshProvisioningType
}

func decodeMeshProvisioning(data []byte, p gopacket.PacketBuilder) error {
	meshProvisioning := &MeshProvisioning{}
	if err := meshProvisioning.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(meshProvisioning)
	return p.NextDecoder(meshProvisioning.NextLayerType())
}

func (mp *MeshProvisioning) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 2 {
		df.SetTruncated()
		return fmt.Errorf("invalid Mesh Provisioning packet. Length %v less than %v", len(data), 2)
	}
	mp.Type = MeshProvisioningType(data[0])

	mp.Contents = data[:1]
	mp.Payload = data[1:]

	return nil
}

func (mp *MeshProvisioning) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(1)
	if err != nil {
		return err
	}
	bytes[0] = uint8(mp.Type)
	return nil
}

// NextLayerType function
func (mp *MeshProvisioning) NextLayerType() gopacket.LayerType {
	return mp.Type.LayerType()
}

// LayerType function
func (mp *MeshProvisioning) LayerType() gopacket.LayerType {
	return LayerMeshProvisioningType
}

type MeshProvisioningInvite struct {
	layers.BaseLayer

	Attention uint8
}

func decodeMeshProvInvite(data []byte, p gopacket.PacketBuilder) error {
	meshProvInvite := &MeshProvisioningInvite{}
	if err := meshProvInvite.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(meshProvInvite)
	return p.NextDecoder(meshProvInvite.NextLayerType())
}

func (mps *MeshProvisioningInvite) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 1 {
		df.SetTruncated()
		return fmt.Errorf("invalid Mesh Provisioning Start packet. Length %v less than %v", len(data), 1)
	}
	mps.Attention = uint8(data[0])

	mps.Contents = data[:1]
	mps.Payload = nil

	return nil
}

func (mps *MeshProvisioningInvite) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(1)
	if err != nil {
		return err
	}
	bytes[0] = mps.Attention
	return nil
}

// NextLayerType function
func (mps *MeshProvisioningInvite) NextLayerType() gopacket.LayerType {
	return gopacket.LayerTypeZero
}

// LayerType function
func (mps *MeshProvisioningInvite) LayerType() gopacket.LayerType {
	return LayerMeshProvisioningType
}
