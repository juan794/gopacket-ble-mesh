package layers

import (
	"encoding/binary"
	"fmt"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type GenericProvAcknowledgement struct {
	layers.BaseLayer

	GPCF    GenericProvisioningType // always 0x01
	Padding uint8
}

func decodeGenericProvAcknowledgement(data []byte, p gopacket.PacketBuilder) error {
	genProvAck := &GenericProvAcknowledgement{}
	if err := genProvAck.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(genProvAck)
	return p.NextDecoder(genProvAck.NextLayerType())
}

func (gpa *GenericProvAcknowledgement) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 1 {
		df.SetTruncated()
		return fmt.Errorf("invalid Generic Provisioning packet. Length %v less than %v", len(data), 1)
	}

	gpa.GPCF = GenericProvisioningType(data[0] & 0x03)
	gpa.Padding = uint8((data[0] >> 2))

	gpa.Contents = data[:1]
	return nil
}

func (gpa *GenericProvAcknowledgement) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(1)
	if err != nil {
		return err
	}
	bytes[0] = (gpa.Padding << 6) | uint8(gpa.GPCF)
	return nil
}

// NextLayerType function
func (gp *GenericProvAcknowledgement) NextLayerType() gopacket.LayerType {
	return gopacket.LayerTypeZero
}

// LayerType function
func (gp *GenericProvAcknowledgement) LayerType() gopacket.LayerType {
	return LayerGenericProvAckType
}

type GenericProvTxStart struct {
	layers.BaseLayer

	GPCF        GenericProvisioningType // always 0x00
	LastSegment uint8
	TotalLength uint16
	FCS         byte
}

func decodeGenericProvTxStart(data []byte, p gopacket.PacketBuilder) error {
	genProvStart := &GenericProvTxStart{}
	if err := genProvStart.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(genProvStart)
	return p.NextDecoder(genProvStart.NextLayerType())
}

func (gps *GenericProvTxStart) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 5 {
		df.SetTruncated()
		return fmt.Errorf("invalid Generic Provisioning packet. Length %v less than %v", len(data), 5)
	}

	gps.GPCF = GenericProvisioningType(data[0] & 0x03)
	gps.LastSegment = uint8((data[0] >> 2))
	gps.TotalLength = binary.BigEndian.Uint16(data[1:3])
	gps.FCS = data[3]

	gps.Contents = data[:4]
	gps.Payload = data[4:]
	return nil
}

func (gps *GenericProvTxStart) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(4)
	if err != nil {
		return err
	}
	bytes[0] = (gps.LastSegment << 6) | uint8(gps.GPCF)
	binary.BigEndian.PutUint16(bytes[1:], gps.TotalLength)
	bytes[3] = gps.FCS

	return nil
}

// NextLayerType function
func (gps *GenericProvTxStart) NextLayerType() gopacket.LayerType {
	if gps.LastSegment > 0 {
		return gopacket.LayerTypeFragment
	}
	return LayerMeshProvisioningType
}

// LayerType function
func (gp *GenericProvTxStart) LayerType() gopacket.LayerType {
	return LayerGenericProvTxStartType
}

type GenericProvTxContinue struct {
	layers.BaseLayer

	GPCF          GenericProvisioningType // always 0x02
	SegmentNumber uint8
}

func decodeGenericProvTxContinue(data []byte, p gopacket.PacketBuilder) error {
	genProvCont := &GenericProvTxContinue{}
	if err := genProvCont.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(genProvCont)
	return p.NextDecoder(genProvCont.NextLayerType())
}

func (gps *GenericProvTxContinue) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 2 {
		df.SetTruncated()
		return fmt.Errorf("invalid Generic Provisioning packet. Length %v less than %v", len(data), 2)
	}

	gps.GPCF = GenericProvisioningType(data[0] & 0x03)
	gps.SegmentNumber = uint8((data[0] >> 2))

	gps.Contents = data[:1]
	gps.Payload = data[1:]
	return nil
}

func (gps *GenericProvTxContinue) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	bytes, err := b.PrependBytes(1)
	if err != nil {
		return err
	}
	bytes[0] = (gps.SegmentNumber << 6) | uint8(gps.GPCF)

	return nil
}

// NextLayerType function
func (gps *GenericProvTxContinue) NextLayerType() gopacket.LayerType {
	return gopacket.LayerTypeFragment
}

// LayerType function
func (gp *GenericProvTxContinue) LayerType() gopacket.LayerType {
	return LayerGenericProvTxContinueType
}
