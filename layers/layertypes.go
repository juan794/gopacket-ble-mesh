package layers

import "github.com/google/gopacket"

const (
	AdvertisingLayerID = iota + 1500
	BeaconLayerID
	UnprovLayerID
	SecureNetworkLayerID
	PBADVLayerID
	GenericProvAckID
	GenericProvTxStartID
	GenericProvTxContinueID
	BearerControlID
	MeshProvisioningID
	MeshProvInviteID
)

var (
	// LayerAdvertisingType var
	LayerAdvertisingType = gopacket.RegisterLayerType(AdvertisingLayerID, gopacket.LayerTypeMetadata{
		Name:    "Avertising Data",
		Decoder: gopacket.DecodeFunc(decodeAdvertising),
	})

	// LayerBeaconType var
	LayerBeaconType = gopacket.RegisterLayerType(BeaconLayerID, gopacket.LayerTypeMetadata{
		Name:    "Beacon",
		Decoder: gopacket.DecodeFunc(decodeBeacon),
	})

	// LayerUnprovisionedType var
	LayerUnprovisionedType = gopacket.RegisterLayerType(UnprovLayerID, gopacket.LayerTypeMetadata{
		Name:    "Unprovisioned Device",
		Decoder: gopacket.DecodeFunc(decodeUnprovisioned),
	})

	// LayerSecureNetworkType var
	LayerSecureNetworkType = gopacket.RegisterLayerType(SecureNetworkLayerID, gopacket.LayerTypeMetadata{
		Name:    "Secure Network Control",
		Decoder: gopacket.DecodeFunc(decodeSecureNetwork),
	})

	// LayerPBADVType var
	LayerPBADVType = gopacket.RegisterLayerType(PBADVLayerID, gopacket.LayerTypeMetadata{
		Name:    "PB-ADV",
		Decoder: gopacket.DecodeFunc(decodePBADV),
	})

	// LayerPBADVType var
	LayerGenericProvAckType = gopacket.RegisterLayerType(GenericProvAckID, gopacket.LayerTypeMetadata{
		Name:    "Generic Provisioning Acknowledgement",
		Decoder: gopacket.DecodeFunc(decodeGenericProvAcknowledgement),
	})

	LayerGenericProvTxStartType = gopacket.RegisterLayerType(GenericProvTxStartID, gopacket.LayerTypeMetadata{
		Name:    "Generic Provisioning TX Start",
		Decoder: gopacket.DecodeFunc(decodeGenericProvTxStart),
	})

	LayerGenericProvTxContinueType = gopacket.RegisterLayerType(GenericProvTxContinueID, gopacket.LayerTypeMetadata{
		Name:    "Generic Provisioning TX Continue",
		Decoder: gopacket.DecodeFunc(decodeGenericProvTxContinue),
	})

	// LayerPBADVType var
	LayerMeshProvisioningType = gopacket.RegisterLayerType(MeshProvisioningID, gopacket.LayerTypeMetadata{
		Name:    "Mesh Provisioning PDU",
		Decoder: gopacket.DecodeFunc(decodeMeshProvisioning),
	})

	// LayerPBADVType var
	LayerMeshProvInviteType = gopacket.RegisterLayerType(MeshProvInviteID, gopacket.LayerTypeMetadata{
		Name:    "Mesh Provisioning Invite",
		Decoder: gopacket.DecodeFunc(decodeMeshProvInvite),
	})
)
