package layers

import "github.com/google/gopacket/layers"

type BearerControl struct {
	layers.BaseLayer

	GPCF   uint8
	Opcode uint8
}
