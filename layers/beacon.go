package layers

import (
	"encoding/hex"
	"fmt"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type BeaconType uint8

const (
	BeaconTypeUnprovisioned = 0
	BeaconTypeSecureNetwork = 1
)

func (b BeaconType) LayerType() gopacket.LayerType {
	switch b {
	case BeaconTypeUnprovisioned:
		return LayerUnprovisionedType
	case BeaconTypeSecureNetwork:
		return LayerSecureNetworkType
	default:
		return gopacket.LayerTypeDecodeFailure
	}
}

type Beacon struct {
	layers.BaseLayer

	Type BeaconType
}

func decodeBeacon(data []byte, p gopacket.PacketBuilder) error {
	beacon := &Beacon{}
	if err := beacon.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(beacon)
	return p.NextDecoder(beacon.NextLayerType())
}

func (b *Beacon) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 2 {
		df.SetTruncated()
		return fmt.Errorf("invalid Beacon packet. Length %v less than %v", len(data), 2)
	}

	b.Type = BeaconType(data[0])

	b.Contents = data[:1]
	b.Payload = data[1:]

	return nil
}

func (be *Beacon) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	return nil
}

// NextLayerType function
func (b *Beacon) NextLayerType() gopacket.LayerType {
	return b.Type.LayerType()
}

// LayerType function
func (b *Beacon) LayerType() gopacket.LayerType {
	return LayerBeaconType
}

type UUID []byte

func (uuid UUID) String() string {
	return hex.EncodeToString(uuid)
}

type Unprovisioned struct {
	layers.BaseLayer

	Device UUID
	OOB    []byte
	URI    []byte
}

func decodeUnprovisioned(data []byte, p gopacket.PacketBuilder) error {
	unprov := &Unprovisioned{}
	if err := unprov.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(unprov)
	return p.NextDecoder(unprov.NextLayerType())
}

// DecodeFromBytes function
func (u *Unprovisioned) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 18 {
		df.SetTruncated()
		return fmt.Errorf("invalid Advertising packet. Length %v less than %v", len(data), 16)
	}

	u.Device = UUID(data[:16])
	u.OOB = data[16:18]

	if len(data) == 18 {
		u.Contents = data[:18]
		return nil
	}

	u.URI = data[18:22]
	u.Contents = data[:22]
	return nil
}

// SerializeTo function
func (u *Unprovisioned) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	return nil
}

// NextLayerType function
func (u *Unprovisioned) NextLayerType() gopacket.LayerType {
	return gopacket.LayerTypeZero
}

// LayerType function
func (u *Unprovisioned) LayerType() gopacket.LayerType {
	return LayerUnprovisionedType
}

type SecureNetwork struct {
	layers.BaseLayer

	Type           uint8
	Flags          uint8
	NetworkID      []byte
	IVIndex        []byte
	Authentication []byte
}

func decodeSecureNetwork(data []byte, p gopacket.PacketBuilder) error {
	secureNetwork := &SecureNetwork{}
	if err := secureNetwork.DecodeFromBytes(data, p); err != nil {
		return err
	}
	p.AddLayer(secureNetwork)
	return p.NextDecoder(secureNetwork.NextLayerType())
}

func (sn *SecureNetwork) DecodeFromBytes(data []byte, df gopacket.DecodeFeedback) error {
	if len(data) < 22 {
		df.SetTruncated()
		return fmt.Errorf("invalid Secure Network packet. Length %v less than %v", len(data), 22)
	}

	sn.Type = uint8(data[0])
	sn.Flags = uint8(data[1])
	sn.NetworkID = data[2:10]
	sn.IVIndex = data[10:14]
	sn.Authentication = data[14:22]

	sn.Contents = data[:22]

	return nil
}

func (sn *SecureNetwork) SerializeTo(b gopacket.SerializeBuffer, opts gopacket.SerializeOptions) error {
	return nil
}

// NextLayerType function
func (sn *SecureNetwork) NextLayerType() gopacket.LayerType {
	return gopacket.LayerTypeZero
}

// LayerType function
func (sn *SecureNetwork) LayerType() gopacket.LayerType {
	return LayerSecureNetworkType
}
