package main

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"math"
	"math/rand"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/google/gopacket"
	"gitlab.com/juan794/gopacket-ble-mesh/layers"
)

type Tag struct {
	Probability int8
	Offset      uint8
	Length      uint8
	BitMask     byte
}

func main() {
	rand.Seed(time.Now().Unix())

	for i := 0; i < 10; i++ {
		payload, _ := hex.DecodeString("0829ce9f8efa000b00")
		newPayload, _ := Mutate(payload)
		fmt.Println(hex.EncodeToString(payload))
		fmt.Println(hex.EncodeToString(newPayload))
		fmt.Println()
	}

}

func Mutate(payload []byte) ([]byte, error) {
	packet := gopacket.NewPacket(payload, layers.LayerAdvertisingType, gopacket.Lazy)
	if err := packet.ErrorLayer(); err != nil {
		return nil, fmt.Errorf("cannot dissect")
	}
	var buf bytes.Buffer
	for _, layer := range packet.Layers() {
		t := reflect.TypeOf(layer).Elem()
		contents := layer.LayerContents()

		base, ok := t.FieldByName("BaseLayer")
		if !ok {
			continue
		}
		layerTag := processTag(base.Tag.Get("fuzzer"))
		rnd := int8(rand.Int31n(100))
		//fmt.Println(t.String(), hex.EncodeToString(layer.LayerContents()), layerTag.Probability, rnd)

		if layerTag.Probability < rnd {
			buf.Write(contents)
			continue
		}
		for i := 0; i < t.NumField(); i++ {
			f := t.FieldByIndex([]int{i})
			if strings.Compare(f.Name, "BaseLayer") == 0 {
				continue
			}

			fieldTag := processTag(f.Tag.Get("fuzzer"))
			rnd := int8(rand.Int31n(100))
			//fmt.Println("\t", f.Name, f.Type.String(), f.Tag, fieldTag.Probability, rnd)

			if fieldTag.Probability < rnd {
				continue
			}
			switch f.Type.Kind() {
			case reflect.Uint8:
				//fmt.Println("\t\t Fuzzing", f.Name, fieldTag.Probability, rnd, fieldTag.Offset)
				contents[fieldTag.Offset] = byte(rand.Int31n(math.MaxUint8))
			}
		}
		buf.Write(contents)
	}
	ret := make([]byte, buf.Len())
	copy(ret, buf.Bytes())
	return ret, nil
}

func processTag(tagStr string) *Tag {
	tag := Tag{Probability: -1}
	if strings.Compare(tagStr, "") == 0 {
		return &tag
	}
	elemtents := strings.Split(tagStr, ";")
	for _, element := range elemtents {
		split := strings.Split(element, ":")
		name, val := split[0], split[1]
		if strings.Compare(name, "prob") == 0 {
			val, err := strconv.Atoi(val)
			if err != nil {
				continue
			}
			tag.Probability = int8(val)
		}
		if strings.Compare(name, "offset") == 0 {
			val, err := strconv.Atoi(val)
			if err != nil {
				continue
			}
			tag.Offset = uint8(val)
		}
		if strings.Compare(name, "length") == 0 {
			val, err := strconv.Atoi(val)
			if err != nil {
				continue
			}
			tag.Length = uint8(val)
		}
		if strings.Compare(name, "bitmask") == 0 {
			val, err := hex.DecodeString(strings.Replace(val, "0x", "", 1))
			if err != nil {
				continue
			}
			tag.BitMask = val[0]
		}
	}
	return &tag
}
