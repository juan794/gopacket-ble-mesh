package main

import (
	"encoding/hex"
	"fmt"
	"os"
	"time"

	"github.com/go-ble/ble"
	"github.com/go-ble/ble/linux/hci"
	"github.com/go-ble/ble/linux/hci/cmd"
)

func main() {
	dev, err := hci.NewHCI([]ble.Option{ble.OptDeviceID(0)}...)
	if err != nil {
		fmt.Printf("[ERR] %v", err)
		os.Exit(1)
	}
	defer dev.Close()
	fmt.Println("Device")

	if err = dev.Init(); err != nil {
		fmt.Printf("[ERR] %v", err)
		os.Exit(1)
	}
	fmt.Println("Init")

	if err := dev.Send(&cmd.LESetAdvertisingParameters{
		AdvertisingIntervalMin:  160,
		AdvertisingIntervalMax:  160,
		AdvertisingType:         0x03,
		OwnAddressType:          0x01,
		DirectAddressType:       0x00,
		AdvertisingChannelMap:   0x07,
		AdvertisingFilterPolicy: 0x03,
	}, nil); err != nil {
		fmt.Printf("[ERR] %v", err)
		os.Exit(1)
	}
	fmt.Println("Enable")

	if err = dev.SetAdvHandler(advHandler); err != nil {
		fmt.Printf("[ERR] %v", err)
		os.Exit(1)
	}
	fmt.Println("Handler")

	if err := dev.Scan(true); err != nil {
		fmt.Printf("[ERR] %v", err)
		os.Exit(1)
	}
	fmt.Println("Scan")

	for {
		payload, err := hex.DecodeString("02010603032718151627186eb5e7409b17414d832efd53772d35650000")
		if err != nil {
			break
		}
		if err := dev.SetAdvertisement(payload, payload); err != nil {
			fmt.Printf("[ERR][1] %v", err)
			os.Exit(1)
		}
		if err := dev.Advertise(); err != nil {
			fmt.Printf("[ERR][2] %v", err)
			os.Exit(1)
		}
		fmt.Printf("Packet Sent: %x\n", payload)
		dev.StopAdvertising()
		time.Sleep(time.Second)
	}
}

func advHandler(a ble.Advertisement) {
	adv, ok := a.(*hci.Advertisement)
	if !ok || adv.EventType() != 3 {
		return
	}
	data := adv.Data()
	fmt.Printf("Packet Received: %v\n", hex.EncodeToString(data))
}
